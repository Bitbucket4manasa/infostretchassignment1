public class PublicTransport extends Q2_Vehicle{

    private String registrationNumber;
    int capacity;

    PublicTransport(){

    }

    PublicTransport(String registrationNumber, int capacity){
        this.registrationNumber=registrationNumber;
        this.capacity=capacity;
    }
    public void service(){

    }

    void start(){
        System.out.print("Public transportation start");
    }

    public static void main(String args[]){

       // Q2_Vehicle transport=new PublicTransport("A65NB",30);
        Q2_Vehicle transport=new PublicTransport();
        transport.start();

    }

}
