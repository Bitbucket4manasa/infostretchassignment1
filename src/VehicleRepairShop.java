import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VehicleRepairShop<Public> {


    private String jobName;
    private String jobStatus;
    private String customerName;
    private String customerPhNum;
    private int customerFreq;
    private float bill;
    private Date date;
    static String shopName;

    VehicleRepairShop()  {

    }
    VehicleRepairShop(String customerName, String customerPhNum, int customerFreq, String jobName, String jobStatus, float bill,Date date)  {

    }

    public String getJobName() {
        return jobName;
    }

    public float getBill(String customerName, int customerFreq) {
        return bill;
    }

    public void setBill(float bill) {
        this.bill = bill;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getCustomerName(String customerPhNum) {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhNum(String customerName) {
        return customerPhNum;
    }

    public void setCustomerPhNum(String customerPhNum) {
        this.customerPhNum = customerPhNum;
    }

    public int getCustomerFreq(String customerPhNum, String customerName) {
        return customerFreq;
    }

    public void setCustomerFreq(int customerFreq) {
        this.customerFreq = customerFreq;
    }

    public int dailyIncome(Date date){

        int income = 0;
        return income;
    }

    public static void main(String args[]) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse("2020-07-26");
        VehicleRepairShop vehicleRepairShop=new VehicleRepairShop("Sushmita","9200992112",2,"Car Wash","Pending", (float) 200.00, date);

        Date date1 = sdf.parse("2020-07-25");
        int dailyIncomeCal=new VehicleRepairShop().dailyIncome(date1);
        System.out.print("Income "+dailyIncomeCal);
    }
}
