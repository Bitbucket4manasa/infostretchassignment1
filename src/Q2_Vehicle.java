public class Q2_Vehicle {

    public String name;
    public double cost;
    public String fuel;
    public int maxSpeed;
    public String transportMedium;

    void start(){
        System.out.println("Vehicle started");
    }
    public void stop(){
        System.out.println("Vehicle stopped");
    }
    public void move(){
        System.out.println("Vehicle moved");
    }
    public void accelerate(){
        System.out.println("Vehicle accelerate");
    }
    public void brake(){
        System.out.println("Vehicle brake");
    }
}
